#### How to run the project

```sh
mkdir -p output && javac ./src/com/lexicalanalyzer/*/*.java -d output && cp -r ./src/com/lexicalanalyzer/resources/ ./output/com/lexicalanalyzer/resources && java -cp output com.lexicalanalyzer.presentation.Main
```

#### How to use

###### Loading data from a file, for instance, loading the test file inside the root folder of the project

- Load the file pressing F1
- Press F4 for processing the data loaded

###### Loading data from the input

- Write whatever you want in the input
- Press F5 for loading the data into lexical analyzer
- Press F4 for processing the data loaded

:warning: If you want to process again the same data you have to press F3 for going back, and if you want to save the data loaded in a file you have to press F2, and the file will be save in the root folder of the project


![Semantic description of image](/screenshots/first.png)Initial view

![Semantic description of image](/screenshots/second.png)File options

![Semantic description of image](/screenshots/third.png)Program options