package com.lexicalanalyzer.business;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class Band {

    private char[] program;
    public int cc = 0;
    public static char EOLN = 10;
    public static char EOF = 3;
    private JTextPane textPane;

    public Band(JTextPane textPane) {
        this.textPane = textPane;
    }

    public void loadProgram(String program) {
        this.program = new char[program.length() + 1];
        this.program[this.program.length - 1] = EOF;

        for (int i = 0; i < program.length(); i++) {
            if (program.charAt(i) == EOLN) {
                this.program[i] = EOLN;
            }

            this.program[i] = program.charAt(i);
        }

        selectRow();
    }

    public void init() {
        this.cc = 0;
        selectRow();
    }

    public void next() {
        try {
            if (cc != program.length - 1) {
                this.cc++;
                selectRow();
                Thread.sleep(100);
            } else {
                System.out.println("Fin de la cinta");
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(Band.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public char currentChar() {
        return program[cc];
    }

    private void selectRow() {
        int begin = cc;
        int end = begin + 1;
        textPane.select(begin, end);
    }

    public void paintText() {
        MutableAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setForeground(attr, Color.red);
        textPane.setCharacterAttributes(attr, false);
    }
    
    public void paintText(int init){
        textPane.select(init, cc);
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(Band.class.getName()).log(Level.SEVERE, null, ex);
        }
        MutableAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setForeground(attr, Color.red);
        textPane.setCharacterAttributes(attr, false);
    }
}
