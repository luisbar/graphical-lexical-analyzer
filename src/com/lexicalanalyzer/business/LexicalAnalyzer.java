package com.lexicalanalyzer.business;

import com.lexicalanalyzer.presentation.MyPanel;
import java.util.HashMap;
import java.util.LinkedList;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class LexicalAnalyzer {

    private HashMap<String, String> listWordsC;
    private LinkedList<String> listI;
    private LinkedList<String> listS;

    private Band band;
    private String token;
    private int vertice;
    private boolean other;
    private int init;

    private JTextField tfPre;
    private JTextField tfLex;

    public LexicalAnalyzer(JTextPane textPane, JTextField tfLex, JTextField tfPre) {
        this.band = new Band(textPane);
        this.token = "";
        this.other = false;
        this.listWordsC = new HashMap<String, String>();
        this.listI = new LinkedList<String>();
        this.listS = new LinkedList<String>();
        this.tfLex = tfLex;
        this.tfPre = tfPre;

        this.listWordsC.put(":=", "<ASSING, _>");
        this.listWordsC.put(")", "<PC, _>");
        this.listWordsC.put("(", "<PA, _>");
        this.listWordsC.put("}", "<LLC, _>");
        this.listWordsC.put("{", "<LLA, _>");
        this.listWordsC.put(";", "<PTCOMA, _>");
        this.listWordsC.put(",", "<COMA, _>");
        this.listWordsC.put("%", "<MOD, _>");
        this.listWordsC.put("*", "<POR, _>");
        this.listWordsC.put("==", "<OPREL, IGUAL>");
        this.listWordsC.put("=<", "<OPREL, MEI>");
        this.listWordsC.put("||", "<OR, _>");
        this.listWordsC.put("<=", "<OPREL, MEI>");
        this.listWordsC.put("<", "<OPREL, MEN>");
        this.listWordsC.put("-", "<OPAR, MENOS>");
        this.listWordsC.put(">", "<OPREL, MAY>");
        this.listWordsC.put(">=", "<OPREL, MAI>");
        this.listWordsC.put("!", "<NOT, _>");
        this.listWordsC.put("/", "<OPAR, DIV>");
        this.listWordsC.put("+", "<OPAR, MAS>");
        this.listWordsC.put("&&", "<AND, _>");
        this.listWordsC.put("=>", "<OPREL, MAI>");
        this.listWordsC.put("<>", "<OPREL, DIS>");
        this.listWordsC.put("><", "<OPREL, DIS>");
        this.listWordsC.put("si", "<SI, _>");
        this.listWordsC.put("sino", "<SINO, _>");
        this.listWordsC.put("proc", "<PROC, _>");
        this.listWordsC.put("main", "<MAIN, _>");
        this.listWordsC.put("return", "<RETURN, _>");
        this.listWordsC.put("print", "<PRINT, _>");
        this.listWordsC.put("read", "<READ, _>");
        this.listWordsC.put("entero", "<ENTERO, _>");
        this.listWordsC.put("boole", "<BOOLEANO, _>");
        this.listWordsC.put("mientras", "<MIENTRAS, _>");
    }

    public void analyzer() {
        tfLex.setText("");
        tfPre.setText("");
        switch (vertice) {
            case 0:
                if (space(band.currentChar())) {
                    vertice = 0;
                    MyPanel.state = 0;
                    break;
                } else {
                    if (band.currentChar() == '"') {
                        token = token + band.currentChar();
                        vertice = 5;
                        MyPanel.state = 2;
                        break;
                    } else {
                        if (letter(band.currentChar())) {
                            token = token + band.currentChar();
                            vertice = 7;
                            MyPanel.state = 18;
                            break;
                        } else {
                            if (digit(band.currentChar())) {
                                token = token + band.currentChar();
                                vertice = 1;
                                MyPanel.state = 1;
                                break;
                            } else {
                                if (band.currentChar() == '=') {
                                    token = token + band.currentChar();
                                    vertice = 9;
                                    MyPanel.state = 4;
                                    break;
                                } else {
                                    if (band.currentChar() == '/') {
                                        vertice = 3;
                                        MyPanel.state = 24;
                                        break;
                                    } else {
                                        if (band.currentChar() == '>') {
                                            token = token + band.currentChar();
                                            vertice = 14;
                                            MyPanel.state = 16;
                                            break;
                                        } else {
                                            if (band.currentChar() == '!') {
                                                token = token + band.currentChar();
                                                vertice = 12;
                                                MyPanel.state = 45;
                                                break;
                                            } else {
                                                if (band.currentChar() == '<') {
                                                    token = token + band.currentChar();
                                                    vertice = 16;
                                                    MyPanel.state = 10;
                                                    break;
                                                } else {
                                                    if (band.currentChar() == '|') {
                                                        token = token + band.currentChar();
                                                        vertice = 25;
                                                        MyPanel.state = 22;
                                                        break;
                                                    } else {
                                                        if (band.currentChar() == '+') {
                                                            token = token + band.currentChar();
                                                            tfLex.setText(token);
                                                            tfPre.setText("<OPAR, mas>");
                                                            vertice = 0;
                                                            token = "";
                                                            MyPanel.state = 20;
                                                            break;
                                                        } else {
                                                            if (band.currentChar() == '-') {
                                                                token = token + band.currentChar();
                                                                tfLex.setText(token);
                                                                tfPre.setText("<OPAR, ->");
                                                                vertice = 0;
                                                                token = "";
                                                                MyPanel.state = 23;
                                                                break;
                                                            } else {
                                                                if (band.currentChar() == '*') {
                                                                    token = token + band.currentChar();
                                                                    tfLex.setText(token);
                                                                    tfPre.setText("<OPAR, por>");
                                                                    vertice = 0;
                                                                    token = "";
                                                                    MyPanel.state = 21;
                                                                    break;
                                                                } else {
                                                                    if (band.currentChar() == '%') {
                                                                        token = token + band.currentChar();
                                                                        tfLex.setText(token);
                                                                        tfPre.setText("<OPLO, mod>");
                                                                        vertice = 0;
                                                                        token = "";
                                                                        MyPanel.state = 6;
                                                                        break;
                                                                    } else {
                                                                        if (band.currentChar() == '&') {
                                                                            token = token + band.currentChar();
                                                                            vertice = 23;
                                                                            MyPanel.state = 11;
                                                                            break;
                                                                        } else {
                                                                            if (band.currentChar() == ',') {
                                                                                token = token + band.currentChar();
                                                                                tfLex.setText(token);
                                                                                tfPre.setText("<COMA, _>");
                                                                                vertice = 0;
                                                                                token = "";
                                                                                MyPanel.state = 31;
                                                                                break;
                                                                            } else {
                                                                                if (band.currentChar() == ';') {
                                                                                    token = token + band.currentChar();
                                                                                    tfLex.setText(token);
                                                                                    tfPre.setText("<PTOCOMA, _>");
                                                                                    vertice = 0;
                                                                                    token = "";
                                                                                    MyPanel.state = 7;
                                                                                    break;
                                                                                } else {
                                                                                    if (band.currentChar() == '{') {
                                                                                        token = token + band.currentChar();
                                                                                        tfLex.setText(token);
                                                                                        tfPre.setText("<LA, _>");
                                                                                        vertice = 0;
                                                                                        token = "";
                                                                                        MyPanel.state = 8;
                                                                                        break;
                                                                                    } else {
                                                                                        if (band.currentChar() == '}') {
                                                                                            token = token + band.currentChar();
                                                                                            tfLex.setText(token);
                                                                                            tfPre.setText("<LC, _>");
                                                                                            vertice = 0;
                                                                                            token = "";
                                                                                            MyPanel.state = 32;
                                                                                            break;
                                                                                        } else {
                                                                                            if (band.currentChar() == '(') {
                                                                                                token = token + band.currentChar();
                                                                                                tfLex.setText(token);
                                                                                                tfPre.setText("<PA, _>");
                                                                                                vertice = 0;
                                                                                                token = "";
                                                                                                MyPanel.state = 9;
                                                                                                break;
                                                                                            } else {
                                                                                                if (band.currentChar() == ')') {
                                                                                                    token = token + band.currentChar();
                                                                                                    tfLex.setText(token);
                                                                                                    tfPre.setText("<PC, _>");
                                                                                                    vertice = 0;
                                                                                                    token = "";
                                                                                                    MyPanel.state = 33;
                                                                                                    break;
                                                                                                } else {
                                                                                                    if (band.currentChar() == ':') {
                                                                                                        token = token + band.currentChar();
                                                                                                        vertice = 9;
                                                                                                        MyPanel.state = 37;
                                                                                                        break;
                                                                                                    } else {
                                                                                                        if (band.currentChar() == Band.EOF) {
                                                                                                            tfPre.setText("<FIN, _>");
                                                                                                            vertice = 0;
                                                                                                            token = "";
                                                                                                            other = true;
                                                                                                            MyPanel.state = 46;
                                                                                                            break;
                                                                                                        } else {
                                                                                                            tfLex.setText("");
                                                                                                            tfPre.setText("<ERROR, _>");
                                                                                                            vertice = 0;
                                                                                                            token = "";
                                                                                                            MyPanel.state = 47;
                                                                                                            break;
                                                                                                        }

                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            case 1:
                if (digit(band.currentChar())) {
                    token = token + band.currentChar();
                    vertice = 1;
                    MyPanel.state = 1;
                } else {
                    if (band.currentChar() == '.') {
                        token = token + band.currentChar();
                        vertice = 39;
                        MyPanel.state = 26;
                    } else {
                        tfLex.setText(token);
                        tfPre.setText("<NUM, _>");
                        vertice = 0;
                        token = "";
                        other = true;
                        MyPanel.state = 25;
                    }
                }
                break;

            case 3:
                if (band.currentChar() == '/') {
                    vertice = 33;
                    MyPanel.state = 13;
                } else {
                    if (band.currentChar() == '*') {
                        vertice = 37;
                        MyPanel.state = 38;
                    } else {
                        tfLex.setText(token);
                        tfPre.setText("<DIV, _>");
                        vertice = 0;
                        token = "";
                        other = true;
                        MyPanel.state = 14;
                    }
                }
                break;

            case 5:
                if (band.currentChar() != '"' && band.currentChar() != Band.EOF && band.currentChar() != Band.EOLN) {
                    token = token + band.currentChar();
                    vertice = 5;
                    MyPanel.state = 2;
                    init++;
                } else {
                    if (band.currentChar() == '"') {
                        listS.add(token);
                        tfLex.setText(token);
                        tfPre.setText("<StringCtte, _>");
                        vertice = 0;
                        token = "";
                        MyPanel.state = 3;
                    } else {
                        tfLex.setText("");
                        tfPre.setText("<ERROR, _>");
//                        vertice = 0;
                        token = "";
                        other = true;
                        MyPanel.state = 29;
                    }
                }
                break;

            case 7:
                if (letter(band.currentChar()) || digit(band.currentChar())) {
                    token = token + band.currentChar();
                    vertice = 7;
                    MyPanel.state = 18;
                } else {
                    if (isWordC()) {
                        tfLex.setText(token);
                        tfPre.setText(listWordsC.get(token));
                        vertice = 0;
                        other = true;
                        token = "";
                        MyPanel.state = 42;
                    } else {
                        if (!listI.contains(token)) {
                            listI.add(token.toLowerCase());
                        }
                        tfLex.setText(token);
                        tfPre.setText("<ID, " + token + ">");
                        vertice = 0;
                        token = "";
                        other = true;
                        MyPanel.state = 42;
                    }
                }
                break;

            case 9:
                if (band.currentChar() == '=') {

                    if (token.equals(":")) {
                        token = token + band.currentChar();
                        tfLex.setText(token);
                        tfPre.setText("<ASIGN, _>");
                        vertice = 0;
                        token = "";
                        MyPanel.state = 36;
                    } else {
                        token = token + band.currentChar();
                        tfLex.setText(token);
                        tfPre.setText("<OPREL, igual>");
                        vertice = 0;
                        token = "";
                        MyPanel.state = 5;
                    }
                } else {
                    if (band.currentChar() == '<') {
                        token = token + band.currentChar();
                        if (!token.equals(":")) {
                            tfLex.setText(token);
                            tfPre.setText("<OPREL, MEI");
                            vertice = 0;
                            token = "";
                            MyPanel.state = 44;
                        } else {
                            tfLex.setText("");
                            tfPre.setText("<ERROR, _>");
                            vertice = 0;
                            token = "";
                            MyPanel.state = 29;
                            other = true;
                        }
                    } else {
                        if (band.currentChar() == '>') {
                            token = token + band.currentChar();
                            if (!token.equals(":")) {
                                tfLex.setText(token);
                                tfPre.setText("<OPREL, MAI>");
                                vertice = 0;
                                token = "";
                                MyPanel.state = 43;
                            } else {
                                tfLex.setText("");
                                tfPre.setText("<ERROR, _>");
                                vertice = 0;
                                token = "";
                                MyPanel.state = 29;
                                other = true;
                            }
                        } else {
                            tfLex.setText("");
                            tfPre.setText("<ERROR, _>");
                            vertice = 0;
                            token = "";
                            other = true;
                            MyPanel.state = 29;
                        }

                    }

                }
                break;

            case 12:
                if (band.currentChar() == '=') {
                    token = token + band.currentChar();
                    tfLex.setText(token);
                    tfPre.setText("<OPREL, DIS>");
                    vertice = 0;
                    token = "";
                    MyPanel.state = 30;
                } else {
                    tfLex.setText(token);
                    tfPre.setText("<NOT, _>");
                    vertice = 0;
                    other = true;
                    token = "";
                    MyPanel.state = 48;
                }
                break;

            case 14:
                if (band.currentChar() == '=') {
                    token = token + band.currentChar();

                    if (!token.equals(":")) {
                        tfLex.setText(token);
                        tfPre.setText("<OPREL, MAI>");
                        vertice = 0;
                        MyPanel.state = 41;
                        token = "";
                    } else {
                        tfLex.setText("");
                        tfPre.setText("<ERROR, _>");
                        vertice = 0;
                        token = "";
                        other = true;
                    }
                } else {
                    if (band.currentChar() == '<') {
                        token = token + band.currentChar();
                        tfLex.setText(token);
                        tfPre.setText("<OPREL, DIS>");
                        vertice = 0;
                        token = "";
                        MyPanel.state = 17;
                    } else {
                        tfLex.setText(token);
                        tfPre.setText("<OPREL, MA>");
                        vertice = 0;
                        token = "";
                        other = true;
                        MyPanel.state = 40;
                    }
                }
                break;

            case 15:
                if (!token.equals(":")) {
                    tfLex.setText(token);
                    tfPre.setText("<OPREL, MAI>");
                    vertice = 0;
                    token = "";
                } else {
                    tfLex.setText("");
                    tfPre.setText("<ERROR, _>");
                    vertice = 0;
                    token = "";
                    other = true;
                }
                break;

            case 16:
                if (band.currentChar() == '=') {
                    token = token + band.currentChar();

                    if (!token.equals(":")) {
                        tfLex.setText(token);
                        tfPre.setText("<OPREL, MEI>");
                        vertice = 0;
                        token = "";
                        MyPanel.state = 34;
                    } else {
                        tfLex.setText("");
                        tfPre.setText("<ERROR, _>");
                        vertice = 0;
                        token = "";
                        other = true;
                    }
                } else {
                    if (band.currentChar() == '>') {
                        token = token + band.currentChar();
                        tfLex.setText(token);
                        tfPre.setText("<OPREL, DIS>");
                        vertice = 0;
                        token = "";
                        MyPanel.state = 39;
                    } else {
                        tfLex.setText(token);
                        tfPre.setText("<OPREL, ME>");
                        vertice = 0;
                        token = "";
                        other = true;
                        MyPanel.state = 15;
                    }
                }
                break;

            case 23:
                if (band.currentChar() == '&') {
                    token = token + band.currentChar();
                    tfLex.setText(token);
                    tfPre.setText("<AND, _>");
                    vertice = 0;
                    token = "";
                    MyPanel.state = 35;
                } else {
                    tfLex.setText("");
                    tfPre.setText("<ERROR, _>");
//                    vertice = 0;
                    token = "";
                    other = true;
                    MyPanel.state = 47;
                }
                break;

            case 25:
                if (band.currentChar() == '|') {
                    token = token + band.currentChar();
                    tfLex.setText(token);
                    tfPre.setText("<OR, _>");
                    vertice = 0;
                    token = "";
                    MyPanel.state = 19;
                } else {
                    tfLex.setText("");
                    tfPre.setText("<ERROR, _>");
                    vertice = 0;
                    token = "";
                    other = true;
                    MyPanel.state = 29;
                }
                break;

            case 33:
                if (band.currentChar() != Band.EOLN && band.currentChar() != Band.EOF) {
                    vertice = 33;
                    MyPanel.state = 13;
                } else {
                    vertice = 0;
                    MyPanel.state = 0;
                }
                break;

            case 37:
                if (band.currentChar() != '*' && band.currentChar() != Band.EOF) {
                    vertice = 37;
                    MyPanel.state = 38;
                    init++;
                } else {
                    if (band.currentChar() == '*') {
                        vertice = 38;
                        MyPanel.state = 12;
                        init++;
                    } else {
                        tfLex.setText("");
                        tfPre.setText("<ERROR, _>");
                        token = "";
                        other = true;
                        MyPanel.state = 29;
                    }
                }
                break;

            case 38:
                if (band.currentChar() == '/') {
                    vertice = 0;
                    MyPanel.state = 0;
                    init++;
                } else {
                    vertice = 37;
                    MyPanel.state = 38;
                }

                break;

            case 39:
                if (digit(band.currentChar())) {
                    token = token + band.currentChar();
                    vertice = 61;
                    MyPanel.state = 27;
                } else {
                    tfLex.setText("");
                    tfPre.setText("<ERROR, _>");
                    vertice = 0;
                    token = "";
                    other = true;
                    MyPanel.state = 29;
                }
                break;

            case 61:
                if (digit(band.currentChar())) {
                    token = token + band.currentChar();
                    vertice = 61;
                    MyPanel.state = 27;

                } else {
                    tfLex.setText(token);
                    tfPre.setText("<NUMR, _>");
                    vertice = 0;
                    token = "";
                    other = true;
                    MyPanel.state = 28;
                }
                break;
        }

        if (MyPanel.state == 47) {

            if (vertice == 23) {
                band.paintText(band.cc - 1);
                vertice = 0;
            } else {
                band.paintText();
            }
        }

        if (MyPanel.state == 29) {
            if (vertice == 5) {
                band.paintText(band.cc - (init + 1));
                vertice = 0;
                init = 0;
            } else {
                if (vertice == 37) {
                    band.paintText(band.cc - (init + 2));
                    vertice = 0;
                    init = 0;
                } else {
                    band.paintText(band.cc - 1);
                    vertice = 0;
                }
            }
        }

        if (other == false) {
            band.next();
        } else {
            other = false;
        }
    }

    public boolean isWordC() {
        if (!token.equals("") && token != null) {
            if (listWordsC.containsKey(token.toLowerCase())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void loadProgram(String program) {
        MyPanel.state = -1;
        this.token = "";
        this.other = false;
        this.listI.clear();
        this.listS.clear();
        this.vertice = 0;
        this.band.loadProgram(program);
    }

    public void init() {
        band.init();
    }

    private boolean space(char charSelected) {
        return (int) charSelected == 32 || (int) charSelected == 9 || (int) charSelected == 10;
    }

    private boolean digit(char charSelected) {
        return (int) charSelected == '0'
                || (int) charSelected == '1'
                || (int) charSelected == '2'
                || (int) charSelected == '3'
                || (int) charSelected == '4'
                || (int) charSelected == '5'
                || (int) charSelected == '6'
                || (int) charSelected == '7'
                || (int) charSelected == '8'
                || (int) charSelected == '9';
    }

    private boolean letter(char charSelected) {
        return charSelected == 'a'
                || charSelected == 'b'
                || charSelected == 'c'
                || charSelected == 'd'
                || charSelected == 'e'
                || charSelected == 'f'
                || charSelected == 'g'
                || charSelected == 'h'
                || charSelected == 'i'
                || charSelected == 'j'
                || charSelected == 'k'
                || charSelected == 'l'
                || charSelected == 'm'
                || charSelected == 'n'
                || charSelected == 'ñ'
                || charSelected == 'o'
                || charSelected == 'p'
                || charSelected == 'q'
                || charSelected == 'r'
                || charSelected == 's'
                || charSelected == 't'
                || charSelected == 'u'
                || charSelected == 'v'
                || charSelected == 'w'
                || charSelected == 'x'
                || charSelected == 'y'
                || charSelected == 'z'
                || charSelected == 'A'
                || charSelected == 'B'
                || charSelected == 'C'
                || charSelected == 'D'
                || charSelected == 'E'
                || charSelected == 'F'
                || charSelected == 'G'
                || charSelected == 'H'
                || charSelected == 'I'
                || charSelected == 'J'
                || charSelected == 'K'
                || charSelected == 'L'
                || charSelected == 'M'
                || charSelected == 'N'
                || charSelected == 'Ñ'
                || charSelected == 'O'
                || charSelected == 'P'
                || charSelected == 'Q'
                || charSelected == 'R'
                || charSelected == 'S'
                || charSelected == 'T'
                || charSelected == 'U'
                || charSelected == 'V'
                || charSelected == 'W'
                || charSelected == 'X'
                || charSelected == 'Y'
                || charSelected == 'Z';
    }
}
