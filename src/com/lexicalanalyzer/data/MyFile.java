package com.lexicalanalyzer.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MyFile {
    
    public MyFile(){
    }
    
    public boolean write(Object object, String name){
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(name)));
            out.writeObject(object);
            out.close();
        } catch (IOException ex) {
            System.out.println("No se puede escribir en el archivo");
            return false;
        }
        
        return true;
    }
    
    public Object read(String name){
        Object object = null;
        
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(name)));
            object = in.readObject();
            in.close();
        } catch (IOException ex) {
            System.out.println("No se encuentra el archivo");
            return -1;
        } catch (ClassNotFoundException ex) {
            System.out.println("No se puede leer el archivo");
            return -1;
        }
        
        return object;
    }
}
