package com.lexicalanalyzer.presentation;

import com.lexicalanalyzer.business.LexicalAnalyzer;
import com.lexicalanalyzer.data.MyFile;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;

public class MainFrm extends JFrame implements MouseListener, KeyListener{
    
    private static final long serialVersionUID = 1L;
    private JMenuItem miOpen;
    private JMenuItem miSave;
    private JMenuItem miInit;
    private JMenuItem miNext;
    private JTextPane tpText;
    private LexicalAnalyzer lexicalA;
    private JMenuItem miLoad;
    private JTextField tfPre;
    private JTextField tfLex;
   
    public MainFrm(){
        initComponents();
        this.lexicalA = new LexicalAnalyzer(tpText, tfLex, tfPre);
    }
    
    private void initComponents(){
        Toolkit tk = getToolkit();
        this.setSize(tk.getScreenSize());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setTitle("Analizador Lexico");
        this.setLayout(new BorderLayout());
        
        addTop();
        addCenter();
        
        this.setVisible(true);
    }
    
    private void addTop(){
        JMenuBar mbMenu = new JMenuBar();
        JMenu mFile = new JMenu("Archivo");
        JMenu mOption = new JMenu("Opcion");
        miOpen = new JMenuItem("Abrir (F1)");
        miSave = new JMenuItem("Guardar (F2)");
        miInit = new JMenuItem("Inicio (F3)");
        miNext = new JMenuItem("Avanzar (F4)");
        miLoad = new JMenuItem("Cargar Programa (F5)");
        
        miOpen.addMouseListener(this);
        miSave.addMouseListener(this);
        miInit.addMouseListener(this);
        miNext.addMouseListener(this);
        miLoad.addMouseListener(this);
        
        miOpen.addKeyListener(this);
        miSave.addKeyListener(this);
        miInit.addKeyListener(this);
        miNext.addKeyListener(this);
        miLoad.addKeyListener(this);
        
        mFile.add(miOpen);
        mFile.add(miSave);
        mOption.add(miInit);
        mOption.add(miNext);
        mOption.add(miLoad);
        mbMenu.add(mFile);
        mbMenu.add(mOption);

        
        this.add(mbMenu, BorderLayout.NORTH);
    }
    
    private void addCenter(){
        tpText = new JTextPane();
        tpText.setText("\n\n\n\n\n\n\n");
        tpText.addKeyListener(this);
        JScrollPane spContainer = new JScrollPane(tpText);
        
        JPanel pCenter = new JPanel();
        pCenter.setLayout(new GridBagLayout());
        
        MyPanel pImage = new MyPanel();
        pImage.setBackground(Color.WHITE);
        pImage.setBorder(new TitledBorder("Diagrama De Transicion"));
        
        JLabel lPre = new JLabel("Preanalisis: ");
        JLabel lLex = new JLabel("Lexema: ");
        tfPre = new JTextField();
        tfLex = new JTextField();
        
        GridBagConstraints gbc = new GridBagConstraints();
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.BOTH;
        
        pCenter.add(spContainer, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        gbc.weighty = 1;
        gbc.insets = new Insets(10, 0, 0, 0);
        gbc.fill = GridBagConstraints.BOTH;
        
        pCenter.add(pImage, gbc);
        gbc.weightx = 0;
        gbc.weighty = 0;
        
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        
        pCenter.add(lPre, gbc);
        
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 1;
        
        pCenter.add(tfPre, gbc);
        gbc.weightx = 0;
        
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        
        pCenter.add(lLex, gbc);
        
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 1;
        
        pCenter.add(tfLex, gbc);
        
        this.add(pCenter, BorderLayout.CENTER);
    }

    private void loadFile() {
        JFileChooser fileChooser = new JFileChooser("/home/luisbar/NetBeansProjects/LexicalAnalyzer");
        int value = fileChooser.showOpenDialog(fileChooser);

        if (value == JFileChooser.APPROVE_OPTION) {
            File fileIn = fileChooser.getSelectedFile();
            Object object = new MyFile().read(fileIn.getName());
            tpText.setText(String.valueOf(object));
            
            lexicalA.loadProgram(tpText.getText());
        }else{
            JOptionPane.showMessageDialog(this, "No selecciono archivo");
        }
    }

    private void saveFile() {
        String name = JOptionPane.showInputDialog("Nombre de archivo");
            
        if(name != null && !name.equals("")){
            new MyFile().write(tpText.getText(), name);
            JOptionPane.showMessageDialog(this, "Archivo guardado");
        }else
            JOptionPane.showMessageDialog(this, "No se guardo el archivo");
    }  

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getSource() == miOpen)
            loadFile();
        
        if(e.getSource() == miSave)
            saveFile();
        
        if(e.getSource() == miInit){
            lexicalA.init();
        }
        
        if(e.getSource() == miNext){
            lexicalA.analyzer();
        }
        
        if(e.getSource() == miLoad){
            lexicalA.loadProgram(tpText.getText());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_F1:
                loadFile();
                break;
            case KeyEvent.VK_F2:
                saveFile();
                break;
            case KeyEvent.VK_F3:
                lexicalA.init();
                break;
            case KeyEvent.VK_F4:
                lexicalA.analyzer();
                break;
            case KeyEvent.VK_F5:
                lexicalA.loadProgram(tpText.getText());
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
