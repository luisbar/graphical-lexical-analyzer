package com.lexicalanalyzer.presentation;

import com.lexicalanalyzer.business.Position;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

    private static final long serialVersionUID = 2L;
    private Image wallpaper;
    private Graphics graphicsWallPaper;
    private int turn;
    public static int state = -1;
    private HashMap<Integer, Position> listPosition;

    public MyPanel() {
        this.turn = 0;
        this.listPosition = new HashMap<Integer, Position>();

        savePositions();

        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                repaint();
            }
        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 100);
    }

    @Override
    public void paint(Graphics g) {
        update(g);
    }

    @Override
    public void update(Graphics g) {

        if (graphicsWallPaper == null) {
            wallpaper = createImage(getWidth(), getHeight());
            graphicsWallPaper = wallpaper.getGraphics();
        }

        graphicsWallPaper.setColor(getBackground());
        graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

        Image aux = loadImage("face1.png");

        graphicsWallPaper.drawImage(aux, 620, 220, this);

        graphicsWallPaper.drawImage(aux, 620, 110, this);

        graphicsWallPaper.drawImage(aux, 540, 110, this);
        graphicsWallPaper.drawImage(aux, 460, 110, this);
        graphicsWallPaper.drawImage(aux, 380, 110, this);
        graphicsWallPaper.drawImage(aux, 300, 110, this);

        graphicsWallPaper.drawImage(aux, 700, 110, this);
        graphicsWallPaper.drawImage(aux, 780, 110, this);
        graphicsWallPaper.drawImage(aux, 860, 110, this);
        graphicsWallPaper.drawImage(aux, 940, 110, this);

        graphicsWallPaper.drawImage(aux, 620, 330, this);

        graphicsWallPaper.drawImage(aux, 540, 330, this);
        graphicsWallPaper.drawImage(aux, 460, 330, this);
        graphicsWallPaper.drawImage(aux, 380, 330, this);
        graphicsWallPaper.drawImage(aux, 300, 330, this);

        graphicsWallPaper.drawImage(aux, 700, 330, this);
        graphicsWallPaper.drawImage(aux, 780, 330, this);
        graphicsWallPaper.drawImage(aux, 860, 330, this);
        graphicsWallPaper.drawImage(aux, 940, 330, this);

        graphicsWallPaper.drawImage(aux, 200, 220, this);
        graphicsWallPaper.drawImage(aux, 1040, 220, this);
        graphicsWallPaper.drawImage(aux, 1000, 155, this);
        graphicsWallPaper.drawImage(aux, 240, 155, this);
        graphicsWallPaper.drawImage(aux, 1000, 285, this);
        graphicsWallPaper.drawImage(aux, 240, 285, this);

        graphicsWallPaper.drawImage(aux, 620, 25, this);

        graphicsWallPaper.drawImage(aux, 540, 25, this);
        graphicsWallPaper.drawImage(aux, 460, 25, this);
        graphicsWallPaper.drawImage(aux, 380, 25, this);
        graphicsWallPaper.drawImage(aux, 300, 25, this);

        graphicsWallPaper.drawImage(aux, 700, 25, this);
        graphicsWallPaper.drawImage(aux, 780, 25, this);
        graphicsWallPaper.drawImage(aux, 860, 25, this);
        graphicsWallPaper.drawImage(aux, 940, 25, this);

        graphicsWallPaper.drawImage(aux, 620, 415, this);//medio abajo

        graphicsWallPaper.drawImage(aux, 540, 415, this);
        graphicsWallPaper.drawImage(aux, 460, 415, this);
        graphicsWallPaper.drawImage(aux, 380, 415, this);
        graphicsWallPaper.drawImage(aux, 300, 415, this);

        graphicsWallPaper.drawImage(aux, 700, 415, this);
        graphicsWallPaper.drawImage(aux, 780, 415, this);
        graphicsWallPaper.drawImage(aux, 860, 415, this);
        graphicsWallPaper.drawImage(aux, 940, 415, this);

        graphicsWallPaper.drawImage(aux, 120, 220, this);
        graphicsWallPaper.drawImage(aux, 40, 220, this);

        graphicsWallPaper.drawImage(aux, 1120, 220, this);

        graphicsWallPaper.drawImage(aux, 1200, 220, this);

        graphicsWallPaper.drawImage(aux, 690, 258, this);
        graphicsWallPaper.drawImage(aux, 1120, 145, this);
        //
        aux = loadImage("fondo.png");
        graphicsWallPaper.drawImage(aux, 0, 0, this);

        g.drawImage(wallpaper, 0, 0, this);

        Image aux2 = null;
        switch (state) {
            case 0:
                eatChar(g, 0);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 0) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 1:
                eatChar(g, 1);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 1) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 2:
                eatChar(g, 2);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 2) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 3:
                eatChar(g, 3);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 3) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 4:
                eatChar(g, 4);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 4) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 5:
                eatChar(g, 5);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 5) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 6:
                eatChar(g, 6);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 6) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 7:
                eatChar(g, 7);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 7) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 8:
                eatChar(g, 8);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 8) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 9:
                eatChar(g, 9);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 9) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 10:
                eatChar(g, 10);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 10) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 11:
                eatChar(g, 11);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 11) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 12:
                eatChar(g, 12);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 12) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 13:
                eatChar(g, 13);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 13) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 14:
                eatChar(g, 14);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 14) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 15:
                eatChar(g, 15);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 15) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 16:
                eatChar(g, 16);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 16) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 17:
                eatChar(g, 17);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 17) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 18:
                eatChar(g, 18);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 18) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 19:
                eatChar(g, 19);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 19) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 20:
                eatChar(g, 20);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 20) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 21:
                eatChar(g, 21);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 21) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 22:
                eatChar(g, 22);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 22) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 23:
                eatChar(g, 23);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 23) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 24:
                eatChar(g, 24);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 24) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 25:
                eatChar(g, 25);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 25) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 26:
                eatChar(g, 26);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 26) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 27:
                eatChar(g, 27);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 27) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;
            case 28:
                eatChar(g, 28);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 28) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 29:
                eatError(g, 29);

                aux2 = loadImage("error1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 29) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 30:
                eatChar(g, 30);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 30) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 31:
                eatChar(g, 31);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 31) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 32:
                eatChar(g, 32);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 32) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 33:
                eatChar(g, 33);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 33) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 34:
                eatChar(g, 34);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 34) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 35:
                eatChar(g, 35);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 35) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 36:
                eatChar(g, 36);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 36) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 37:
                eatChar(g, 37);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 37) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 38:
                eatChar(g, 38);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 38) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 39:
                eatChar(g, 39);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 39) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 40:
                eatChar(g, 40);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 40) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 41:
                eatChar(g, 41);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 41) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 42:
                eatChar(g, 42);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 42) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 43:
                eatChar(g, 43);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 43) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 44:
                eatChar(g, 44);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 44) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 45:
                eatChar(g, 45);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 45) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 46:
                eatChar(g, 46);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 46) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 47:
                eatError(g, 47);

                aux2 = loadImage("error1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 47) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;

            case 48:
                eatChar(g, 48);

                aux2 = loadImage("face1.png");
                for (int i = 0; i < listPosition.size(); i++) {
                    if (i != 48) {
                        graphicsWallPaper.drawImage(aux2, listPosition.get(i).getP1(), listPosition.get(i).getP2(), this);
                    }
                }
                break;
        }
        aux = loadImage("fondo.png");
        graphicsWallPaper.drawImage(aux, 0, 0, this);
        g.drawImage(wallpaper, 0, 0, this);
    }

    private Image loadImage(String nombre) {
        File file = null;
        String path = System.getProperty("user.dir") + "/src/com/lexicalanalyzer/resources/images/";
        try {
            file = new File(path + nombre);
            return ImageIO.read(file);
        } catch (Exception e) {
            System.out.println("No se pudo cargar la imagen " + nombre + " de " + file.getPath());
            return null;
        }
    }

    private void savePositions() {
        listPosition.put(0, new Position(620, 220));
        listPosition.put(1, new Position(620, 110));
        listPosition.put(2, new Position(540, 110));
        listPosition.put(3, new Position(460, 110));
        listPosition.put(4, new Position(380, 110));
        listPosition.put(5, new Position(300, 110));
        listPosition.put(6, new Position(700, 110));
        listPosition.put(7, new Position(780, 110));
        listPosition.put(8, new Position(860, 110));
        listPosition.put(9, new Position(940, 110));
        listPosition.put(10, new Position(620, 330));
        listPosition.put(11, new Position(540, 330));
        listPosition.put(12, new Position(460, 330));
        listPosition.put(13, new Position(380, 330));
        listPosition.put(14, new Position(300, 330));
        listPosition.put(15, new Position(700, 330));
        listPosition.put(16, new Position(780, 330));
        listPosition.put(17, new Position(860, 330));
        listPosition.put(18, new Position(940, 330));
        listPosition.put(19, new Position(200, 220));
        listPosition.put(20, new Position(1040, 220));
        listPosition.put(21, new Position(1000, 155));
        listPosition.put(22, new Position(240, 155));
        listPosition.put(23, new Position(1000, 285));
        listPosition.put(24, new Position(240, 285));
        listPosition.put(25, new Position(620, 25));
        listPosition.put(26, new Position(540, 25));
        listPosition.put(27, new Position(460, 25));
        listPosition.put(28, new Position(380, 25));
        listPosition.put(29, new Position(300, 25));
        listPosition.put(30, new Position(700, 25));
        listPosition.put(31, new Position(780, 25));
        listPosition.put(32, new Position(860, 25));
        listPosition.put(33, new Position(940, 25));
        listPosition.put(34, new Position(620, 415));
        listPosition.put(35, new Position(540, 415));
        listPosition.put(36, new Position(460, 415));
        listPosition.put(37, new Position(380, 415));
        listPosition.put(38, new Position(300, 415));
        listPosition.put(39, new Position(700, 415));
        listPosition.put(40, new Position(780, 415));
        listPosition.put(41, new Position(860, 415));
        listPosition.put(42, new Position(940, 415));
        listPosition.put(43, new Position(120, 220));
        listPosition.put(44, new Position(40, 220));
        listPosition.put(45, new Position(1120, 220));
        listPosition.put(46, new Position(1200, 220));
        listPosition.put(47, new Position(690, 258));
        listPosition.put(48, new Position(1120, 145));
    }

    private void eatChar(Graphics g, int state) {
        Image aux = null;
        if (turn == 0) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("face1.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn == 1) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("face2.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn == 2) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("face3.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn == 3) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("face4.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn != 3) {
            turn++;
        } else {
            turn = 0;
        }
    }

    private void eatError(Graphics g, int state) {
        Image aux = null;
        if (turn == 0) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("error1.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn == 1) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("error2.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn == 2) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("error3.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn == 3) {
            graphicsWallPaper.setColor(getBackground());
            graphicsWallPaper.fillRect(0, 0, getWidth(), getWidth());

            aux = loadImage("error4.png");
            graphicsWallPaper.drawImage(aux, listPosition.get(state).getP1(), listPosition.get(state).getP2(), this);
            g.drawImage(wallpaper, 0, 0, this);
        }

        if (turn != 3) {
            turn++;
        } else {
            turn = 0;
        }
    }
}
